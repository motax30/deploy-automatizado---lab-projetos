# Deploy Automatizado - Lab Projetos

Projeto para a disciplina de Laboratório de Projeto de Banco de Dados

# Acesso a Maquina Virtual (Azure) 

IP: 13.84.149.251

Para o acesso a maquina, primeiro precisamos baixar o programa PUTTY. 
    Ele se encontra aqui - https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html
    Vá até a parte de "Alternative binary files" e baixe a versão que vá de acordo com a sua versão do computador (32 ou 64bits) 
    
    Feito isso, abra o programa e no campo de "Host Name(or IP adress)" e coloque o IP mencionado logo acima. 
    
    Clique em Open. 
    
    Ele vai solicitar uma permissão, pode clicar em SIM. 
    
    Vai aparecer uma tela preta solicitando o login, você coloca o seguinte. 
    
        Login: fatec
    Apertando enter, vai solicitar então a senha. 
        Senha: No Grupo do WhatsApp 
    Pronto esta feito o login na maquina virtual. 
    
    Dados da maquina: 
    2 vcpus - 4GB RAM - 30GB SSD