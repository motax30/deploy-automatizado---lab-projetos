package br.edu.fatec.exercicio1labv.rest;

import java.util.Collection;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import br.edu.fatec.exercicio1labv.entities.Carro;
import br.edu.fatec.exercicio1labv.services.impl.CarroServiceImpl;
import br.gov.sp.fatec.view.View;

@RestController
@RequestMapping(value = "/carros")
public class CarroController {
	
	@Autowired
	private CarroServiceImpl carroService;
	
	@RequestMapping(value = "/getById")
	@JsonView(View.CarroCompleto.class)
	public ResponseEntity<Carro> getbyId(@RequestParam(value = "id", defaultValue = "1") Long id){
		Optional<Carro> carro = carroService.findById(id);
		if(!carro.isPresent()) {
			return new ResponseEntity<Carro>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Carro>(carro.get(), HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/getAll")
	@JsonView(View.CarroCompleto.class)
	public ResponseEntity<Collection<Carro>> get(){
		return new ResponseEntity<Collection<Carro>>(carroService.findAll(), HttpStatus.OK);
	}
	
	
	@PostMapping()
	@JsonView(View.CarroCompleto.class)
	public Carro save(@RequestBody @Valid Carro carro, HttpServletRequest request, HttpServletResponse response) {
		carro = carroService.salvar(carro);
		response.addHeader("Location", request.getServerName() +
				":" + request.getServerPort() + request.getContextPath()+
				"/carro/getById?id="+ carro.getId());
		return carro;
	}
	
	@DeleteMapping()
	@JsonView(View.CarroCompleto.class)
	public Carro remover(@RequestBody Carro carro) {
		carroService.remover(carro);
		return carro;
	}
	
	@PutMapping("{id}")
	@JsonView(View.CarroCompleto.class)
	public Carro editar(@PathVariable(value = "id") Long id, @RequestBody @Valid Carro carro) {
		carroService.atualizar(id, carro);
		return carro;
	}
	
	
}
